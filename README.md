# Terraform AWS LAB

En este proyecto se pueden encontrar archivos de Terraform (.tf) para desplegar una infraestructura en AWS que alojará un sitio web con un formulario que almacenará información del usuario en una instancia RDS MySQL y se cuenta con los siguientes componentes:


   * Nueva VPC
   * 2 subnets privadas
   * 1 subnet publica
   * 1 Elastic Load Balancer
   * 2 instancias EC2 (servidores web)
   * 1 instancia RDS MySQL

Pueden encontrar más información sobre el laboratorio en [Sysadmin Dojo] (http://omartinex.com/infraestructura/infraestructura-como-codigo-con-terraform-parte-1/)

## Requisitos:

   * Cuenta en AWS (puede ser free tier)
   * Terraform v0.8.7 o superior
   * Editor de textos (vi/vim, nano, atom, sublime, etc.)

 
