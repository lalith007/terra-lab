variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "eu-west-2"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        eu-west-2 = "ami-b6daced2" # amzn-ami-hvm-2017.03.0.20170417-x86_64-gp2
    }
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.0.0/24"
}

variable "private_subnet_a_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.1.0/24"
}

variable "private_subnet_b_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.2.0/24"
}
