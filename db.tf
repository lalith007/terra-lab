resource "aws_db_subnet_group" "omxsubnetgrp" {
  name       = "omxsubnetgrp"
  subnet_ids = ["${aws_subnet.eu-west-2b-private.id}", "${aws_subnet.eu-west-2a-private.id}"]

  tags {
    Name = "omxsubnetgrp"
  }
}

resource "aws_db_instance" "omxdb" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.6.29"
  instance_class       = "db.t2.micro"
  name                 = "omxdb"
  username             = "omxdbuser"
  password             = "omxdbpass"
#  db_subnet_group_name = "${aws_subnet.eu-west-2b-private.id}"
  db_subnet_group_name = "${aws_db_subnet_group.omxsubnetgrp.id}"
  parameter_group_name = "default.mysql5.6"
# security_group_names = ["sgrds"]
  vpc_security_group_ids = ["${aws_security_group.sgrds.id}"]
}
